create table person (
    id varchar(26) primary key
);

create table authentication (
    token varchar(26) primary key,
    person varchar(26) not null
);

create table payment (
    token varchar(26) primary key,
    record_time char(19) not null,
    sender varchar(26) not null,
    contact varchar(26) not null,
    recipient varchar(26),
    timestamp char(19) not null,
    value integer not null,
    foreign key(sender) references person(id),
    foreign key(recipient) references person(id)
);

create table contact (
    id varchar(26) primary key,
    contact varchar(26),
    foreign key(contact) references person(id)
);

create table contact_info (
    contact varchar(26) primary key,
    person varchar(26),
    description varchar(256) not null,
    foreign key(person) references person(id)
);

insert into person values ('root');
insert into person values ('person-abc');
insert into person values ('person-cdf');
insert into authentication values ('a-good-token', 'root');
insert into contact values ('contact-1', 'person-abc');
insert into contact_info values ('contact-1', 'root', 'seth@delisle.cx');
insert into payment values (
    'payment-xyz', '2017-12-04T21:05:18', 'root', 'contact-2', null, '2017-01-01T00:00:00', 600
);
insert into payment values (
    'payment-yzf', '2017-12-04T21:05:18', 'root', 'contact-1', 'person-abc', '2017-01-01T00:00:00',
    600
);
