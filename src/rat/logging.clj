(ns rat.logging
  (:require [taoensso.timbre.appenders.core :as appenders]
            [taoensso.timbre :as timbre :refer [refer-timbre]])) 


(refer-timbre)


(def logfile (or (System/getenv "LOGFILE") "test.log"))


(timbre/merge-config!
  {:appenders {:spit (appenders/spit-appender {:fname logfile})}})


(Thread/setDefaultUncaughtExceptionHandler
 (reify Thread$UncaughtExceptionHandler
   (uncaughtException [_ thread ex]
     (error ex "Uncaught exception on" (.getName thread)))))
