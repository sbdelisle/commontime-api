(ns rat.dynamo
  (:require 
    [taoensso.faraday :as far])) 


(def client-opts {:access-key "..."
                  :secret-key "..."
                  :endpoint "http://localhost:8000"})


; (far/create-table
;   client-opts
;   :authentication
;   [:token :s])
; 
; (far/put-item client-opts :authentication {:token "abc" :person "bob"})
; (far/get-item client-opts :authentication {:token "abc"})


; (far/create-table
;   client-opts
;   :music
;   [:artist :s]
;   {:range-keydef [:song-title :s]}
;   :throughput {:read 1 :write 1}
;   :block? true)
; 
; 
; (far/describe-table client-opts :music)
; 
; (far/list-tables client-opts)
; 
; (far/put-item client-opts
;               :music
;               {:artist      "No One You Know"
;                :song-title  "Call Me Today"
;                :album-title "Somewhat Famous"
;                :year        2015
;                :price       2.14
;                :genre       :country
;                :tags        {:composers ["Smith" "Jones" "Davis"] 
;                              :length-in-seconds 214}})
; 
; (far/get-item client-opts :music {:artist "No One You Know"
;                                   :song-title "Call Me Today"})
; 
; (far/scan client-opts :music)
