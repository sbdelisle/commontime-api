(ns rat.demurrage
  (:require [clj-time.core :refer [now in-seconds interval latest earliest before?]]
            [clojure.math.numeric-tower :as m])) 

(def lunar-month-days 29.530587981)
(def lunar-month-seconds (m/floor (* lunar-month-days 24 60 60)))
(def month-decay-ratio 0.9)


(defn demurrage
  ([quantity] [(now) (int quantity)])
  ([ts quantity] [ts (int quantity)])
  ([ts quantity unit]
   [ts (int (* quantity (get {"hours" 3600 "minutes" 60 "seconds" 1} unit)))] ))


(defn timestamp [d] (first d))


(defn quantity [d] (second d))


(defn decay
  [quantity seconds-elapsed]
  (int (* quantity (m/expt month-decay-ratio (/ seconds-elapsed lunar-month-seconds)))))


(defn quantity-at [d at-ts]
  (let
    [start (earliest (timestamp d) at-ts)
     end (latest (timestamp d) at-ts)
     sign (if (before? (timestamp d) at-ts) 1 -1)]
    (decay (quantity d) (* sign (in-seconds (interval start end))))))


(defn sum 
  [& args]
  (let [base-ts (latest (map timestamp args))]
    [base-ts (reduce + (map #(quantity-at % base-ts) args))]))
