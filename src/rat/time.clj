(ns rat.time
  (:require [clj-time.format :as f]
            [clj-time.core :refer [now days ago]]))

(def rat-format (f/formatters :date-hour-minute-second))

(defn timestamp-now []
 (f/unparse rat-format (now)))

(defn timestamp-90d []
  (f/unparse rat-format (ago (days 90))))

(defn unparse [s]
  (f/unparse rat-format s))


(defn parse [s]
  (f/parse rat-format s))
