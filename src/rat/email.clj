(ns rat.email
  (require [rat.logging]
           [taoensso.timbre :as timbre :refer [refer-timbre]]
           [clj-http.client :as client]
           [clojure.math.numeric-tower :as math])) 

(refer-timbre)

(def postmark-token (or (System/getenv "POSTMARK_TOKEN") "POSTMARK_API_TEST"))
(info "Using POSTMARK Token" postmark-token )


(defn send-email-via-postmark [{recipient :recipient subject :subject text :text}]
  (client/post 
    "https://api.postmarkapp.com/email"
    {:headers {:x-postmark-server-token postmark-token}
     :content-type "application/json"
     :form-params {:From "CommonTime Notification <noreply@commontime.trade>"
                   :To recipient
                   :Subject subject 
                   :TextBody text}}))


(defn display-as-jiffies [seconds]
  (let [jiffies (float (/ seconds 360))
        magnitude (math/abs jiffies)
        format-str (cond
                     (>= magnitude 9.95) "%.0f"
                     (>= magnitude 0.995) "%.1f"
                     :else "%.2f")]
    (format format-str jiffies)))


(defn send-invite-contents [payment contact]
  (let [display-value (display-as-jiffies (:value payment))]
    {:recipient contact
     :subject (str "CommonTime payment of " display-value " jiffies")
     :text (str "A CommonTime user has sent you a payment of " display-value
                " jiffies at " (:timestamp payment) ". "
                "You can claim your payment by visiting the following url:\n\n"
                "\t\thttps://www.commontime.trade/#/claim/" (:token payment))}))


(defn send-invite! [payment contact]
  (let [contents (send-invite-contents payment contact)]
     (future (do
       (info "Sending invite email to " contact)
       (info "Postmark response: " (send-email-via-postmark contents))))))
