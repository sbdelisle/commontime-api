(ns rat.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [rat.time :refer [timestamp-now]]
            [rat.demurrage :as dem]
            [rat.database :refer [get-payments authenticate-token record-claim
                                  record-payment get-all-contact-info
                                  add-contact-info]]
            [schema.core :as s]))


(s/defschema PaymentToRegister
  {:value s/Int
   :timestamp s/Str
   :token s/Str
   (s/optional-key :recipient) s/Str
   (s/optional-key :contact) s/Str
   (s/optional-key :unit) (s/enum "hours" "minutes" "seconds")
   (s/optional-key :memo) s/Str})


(s/defschema StoredPayment
  {:value s/Int
   :timestamp s/Str
   :token s/Str
   :sender s/Str
   (s/optional-key :recipient) s/Str
   (s/optional-key :contact) s/Str
   :record_time s/Str
   (s/optional-key :memo) s/Str})


(s/defschema ContactInfo
  {:id s/Str
   (s/optional-key :person) s/Str
   :description s/Str})


(s/defschema PaymentContact
  {:token s/Str
   :contact s/Str})


(def app
  (api
    {:swagger
     {:ui "/api"
      :spec "/api/swagger.json"
      :data {:info {:title "Rat"
                    :description "Rat API"}
             :tags [{:name "api", :description "Rat API"}]}}} 

    (context
      "/api" []
       :header-params [{x-auth-token :- s/Str "Authentication token"}]


       (POST "/claim" []
        :return {:ok s/Bool}
        :query-params [{token :- s/Str nil}]
        :summary "Registers a claim of a payment"
        (let [person (authenticate-token x-auth-token)] 
          (let [result (record-claim person x-auth-token token)]
            (case (:error result)
              :token-claimed (forbidden result)
              :unknown-token (not-found result)
              nil (ok {:ok true}))))) 


       (GET "/contact-info" []
         :return {:ok s/Bool
                  :contact-info [ContactInfo]}
         :summary "Get contact info"
         (let [person (authenticate-token x-auth-token)]
          (if (nil? person)
            (forbidden {:error "Invalid authentication token"})
            (ok {:ok true :contact-info (get-all-contact-info person)})))) 


       (POST "/contact-info" []
        :return {:ok s/Bool}
        :body [contact-info ContactInfo]
        :summary "Add new contact info"
        (let [person (authenticate-token x-auth-token)]
          (if (nil? person)
            (forbidden {:error "Invalid authentication token"})
            (do (add-contact-info person contact-info) (ok {:ok true })))))


       (GET "/payment" []
         :return {:ok s/Bool :your-id s/Str :payments [StoredPayment]}
         :summary "List payments"
         (let [person (authenticate-token x-auth-token)]
          (if (nil? person)
            (forbidden {:error "Invalid authentication token"})
            (ok {:ok true
                 :your-id person 
                 :payments (get-payments person)}))))

 
        (POST "/payment" []
          :return {:ok s/Bool :message s/Str}
          :body [payment PaymentToRegister]
          :summary "Registers a payment"
          (let [{:keys [timestamp value unit contact]} payment
                token (dem/demurrage timestamp value unit)
                record-time (timestamp-now)
                person (authenticate-token x-auth-token)]
            (if (nil? person)
              (forbidden {:error "Invalid authentication token"})
              (let [result (record-payment
                             (assoc (select-keys payment [:id :token :timestamp :recipient])
                                    :sender person
                                    :value (dem/quantity token)
                                    :record_time record-time)
                             contact)]
                (println "result" result (type result))
                (ok {:ok (not (:error result)) :message (str (:message result))}))))))))
