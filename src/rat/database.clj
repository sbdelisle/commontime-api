(ns rat.database
  (:require
    [alphabase.base58 :as base58]
    [crypto.random :as random]
    [rat.time :as rtime]
    [rat.demurrage :as dem]
    [rat.email :as email]
    [clj-time.core :refer [now]]
    [hugsql.core :as hugsql]))


(defn new-id [] (base58/encode (random/bytes 16)))


(def db
  {:classname "org.postgresql.Driver"
   :subprotocol "postgresql"
   :subname (str "//localhost/" (System/getProperty "user.name"))})


(hugsql/def-db-fns "rat/database.sql")


(defn calculate-balance [payments person-id]
  (let [debits (filter #(= person-id (:sender %)) payments)
        credits (filter #(= person-id (:recipient %)) payments)
        dem-debits (map #(dem/demurrage (rtime/parse (:timestamp %)) (:value %) "seconds")
                        debits)
        dem-credits (map
                      #(dem/demurrage (rtime/parse (:timestamp %)) (:value %) "seconds")
                      credits)
        shared-now (now)]
  (-
   (dem/quantity-at (apply dem/sum (concat [(dem/demurrage 0)] dem-credits)) shared-now)
   (dem/quantity-at (apply dem/sum (concat [(dem/demurrage 0)] dem-debits)) shared-now))))


; used in handler
(defn authenticate-token [token]
  (:person (person-from-auth-token db {:token token})))


(defn remove-items-with-nil-values [payment]
  (into {} (for [[k v] payment :when (not (nil? v))] [k v])))


; used in handler
(defn get-payments [person]
  (map remove-items-with-nil-values (db-get-payments db {:person person})))


; used in handler
(defn add-contact-info [person contact-info]
  (insert-contact db (assoc contact-info :person person)))


; used in handler
(defn get-all-contact-info [person]
  (concat
    (map remove-items-with-nil-values (db-get-contacts db {:person person}))
    [{:id "__TREASURER__" :person "__TREASURER__" :description "TREASURER"}]))


; used in handler
(defn record-claim [person auth-token claim-token]
  (println "entering record-claim")
  (let [person-id (if (nil? person) (new-id) person)
        contact (db-get-contact-for-payment db {:payment_token claim-token})]
    (println "person-id, contact:" person-id contact)
    ; FIXME: do this in a transaction
    (when (nil? person)
      (insert-person db {:id person-id})
      (insert-authentication db {:token auth-token :person person-id}))
    (doseq [payment (map #(first (vals %)) (get-payments-for-contact db {:contact (:id contact)}))]
      (relate-payment-to-recipient db {:payment payment :recipient person-id}))
    (relate-contact-to-person db {:contact (:id contact) :person person-id})))


(defn insufficient-funds? [payment]
  (let [sender (:sender payment)
        resulting-balance (calculate-balance (conj (get-payments sender) payment) sender)]
    (not (or (= sender "__TREASURER__") (>= resulting-balance 0)))))


(defn check-payment-constraints [payment]
  (println "payment" payment)
  (cond
    (<= (:value payment) 0) {:error true :message "Payments must be greater than 0."}
    (= (:sender payment) (:recipient payment)) {:error true :message "Can't pay yourself."}
    (insufficient-funds? payment) {:error true :message "Can't spend to balance below 0."}
    :else {:error false}))


; used in handler
(defn record-payment [payment contact]
  (let [{error :error message :message :as error-info} (check-payment-constraints payment)]
    (println "record-payment: error, message" error message)
    (if error
      error-info
      (do
        ;FIXME: do this in a transaction
        (insert-payment db payment)
        (cond
          (:recipient payment) (relate-payment-to-recipient db {:payment (:token payment)
                                                                :recipient (:recipient payment)})
          contact (do
                    (relate-payment-to-contact
                      db
                      {:payment_token (:token payment) :contact contact})
                    (email/send-invite!
                      payment
                      (:description (db-get-contact-description db {:contact contact})))))
        {:error false}))))

(defn init-sql []
  (println "creating person table")
  (create-person-table db)
  (println "creating contact table")
  (create-contact-table db)
  (println "creating payment table")
  (create-payment-table db)
  (println "creating payment_to_recipient table")
  (create-payment-to-recipient-table db)
  (println "creating payment_to_contact table")
  (create-payment-to-contact-table db)
  (println "creating contact_to_person table")
  (create-contact-to-person-table db)
  (println "creating authentication table")
  (create-authentication-table db))


(defn create-treasurer []
  (let [root-id "__TREASURER__"]
    (insert-person db {:id "__TREASURER__"})
    (insert-authentication db {:token (new-id) :person root-id})))


(defn init-data []
  (let [root-id "__TREASURER__"
        person-1 (new-id)
        person-2 (new-id)
        contact-1 (new-id)
        contact-2 (new-id)
        contact-3 (new-id)
        payment-1 (new-id)
        payment-2 (new-id)
        payment-3 (new-id)]

    (println "adding people")
    (doseq [person [root-id person-1 person-2]] (insert-person db {:id person}))

    (println "adding auth")
    (insert-authentication db {:token "a-good-token" :person root-id})

    (println "adding contacts")
    (insert-contact db {:id contact-1
                        :person root-id
                        :description "don@draper.com"})
    (relate-contact-to-person db {:person person-1 :contact contact-1})

    (insert-contact db {:id contact-2
                        :person root-id
                        :description "arthur@roundtable.gov"})
    (relate-contact-to-person db {:person person-2 :contact contact-2})

    (insert-contact db {:id contact-3
                        :person root-id
                        :description "tony@tiger.af.mil"})

    (println "adding payments")
    (insert-payment db {:token payment-1
                        :record_time "2017-12-04T21:05:18"
                        :sender root-id
                        :timestamp "2017-01-01T00:00:00",
                        :value 450})
    (println "relating first paymen to recipient")
    (relate-payment-to-recipient db {:payment payment-1 :recipient person-1})

    (println "inserting next payment")
    (insert-payment db {:token payment-2
                        :record_time "2017-12-14T21:05:18"
                        :sender root-id
                        :timestamp "2017-06-01T00:00:00"
                        :value 600})
    (relate-payment-to-recipient db {:payment payment-2 :recipient person-2})

    (println "inserting payment with only contact")
    (insert-payment db {:token payment-3
                        :record_time "2017-12-06T21:05:18"
                        :sender root-id
                        :timestamp "2017-05-01T00:00:00"
                        :value 300})
    (println "relating to contact")
    (println "payment, contact 3: " payment-3 contact-3 "payment, contact 2:" payment-2 contact-2)
    (relate-payment-to-contact db {:payment_token payment-3 :contact contact-3})
    (println "done loading data")))


(defn setup-prod-db []
  (init-sql)
  (create-treasurer))


(defn reset-test-db []
  (drop-tables db)
  (init-sql)
  (init-data))

;(reset--test-db)
