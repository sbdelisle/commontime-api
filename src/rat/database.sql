-- :name create-person-table
-- :command :execute
create table if not exists person (
    id varchar(22) primary key
);


-- :name insert-person
-- :command :execute
insert into person values (:id);


-- :name create-authentication-table
-- :command :execute
create table if not exists authentication (
    token varchar(22) primary key,
    person varchar(22) not null
);


-- :name insert-authentication
-- :command :execute
insert into authentication values (:token, :person);


-- :name person-from-auth-token 
-- :command :query
-- :result :1
select person from authentication where token = :token


-- :name create-payment-table
-- :command :execute
create table if not exists payment (
    token varchar(22) primary key,
    record_time char(19) not null,
    sender varchar(22) not null,
    timestamp char(19) not null,
    value integer not null,
    foreign key(sender) references person(id)
);


-- :name insert-payment
-- :command :execute
insert into payment values (:token, :record_time, :sender, :timestamp, :value);


-- :name db-get-payments
-- :command :query
select token, record_time, sender, timestamp, value,
       payment_to_recipient.recipient as recipient,
       payment_to_contact.contact as contact
    from payment
    left join payment_to_recipient on payment.token = payment_to_recipient.payment
    left join payment_to_contact on payment.token = payment_to_contact.payment_token
    where sender = :person or recipient = :person;


-- :name create-contact-table
-- :command :execute
create table if not exists contact (
    id varchar(22) primary key,
    person varchar(22) not null,
    description varchar(256) not null,
    foreign key(person) references person(id)
);


-- :name insert-contact
-- :command :execute
insert into contact values (:id, :person, :description);


-- :name create-payment-to-recipient-table
-- :command :execute
create table if not exists payment_to_recipient (
    payment varchar(22) primary key,
    recipient varchar(22) not null,
    foreign key(payment) references payment(token),
    foreign key(recipient) references person(id)
);


-- :name relate-payment-to-recipient
-- :command :execute
insert into payment_to_recipient values (:payment, :recipient)


-- :name create-contact-to-person-table
-- :command :execute
create table if not exists contact_to_person (
    contact varchar(22) primary key,
    person varchar(22) not null,
    foreign key(contact) references contact(id),
    foreign key(person) references person(id)
);


-- :name relate-contact-to-person
-- :command :execute
insert into contact_to_person values (:contact, :person);


-- :name create-payment-to-contact-table
-- :command :execute
create table if not exists payment_to_contact (
    contact varchar(22),
    payment_token varchar(22),
    primary key(contact, payment_token),
    foreign key(contact) references contact(id),
    foreign key(payment_token) references payment(token)
);


-- :name relate-payment-to-contact
-- :command :execute
insert into payment_to_contact values (:contact, :payment_token);


-- :name get-payments-for-contact
-- :command :query
select payment_token from payment_to_contact where contact = :contact


-- :name db-get-contacts
-- :command :query
select contact.id, contact.description, contact_to_person.person as person
    from contact
    left join contact_to_person on contact_to_person.contact = contact.id
    where contact.person = :person;


-- :name db-get-contact-description
-- :command :query
-- :result :1
select contact.id, contact.description from contact where contact.id = :contact


-- :name db-get-contact-for-payment
-- :command :query
-- :result :1
select contact as id from payment_to_contact where payment_token = :payment_token;


-- :name drop-tables
-- :command :execute
drop table if exists contact_to_person cascade;
drop table if exists payment_to_contact cascade;
drop table if exists payment_to_recipient cascade;
drop table if exists contact cascade;
drop table if exists payment cascade;
drop table if exists person cascade;
drop table if exists authentication;
