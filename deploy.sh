SRC_DIR=$(dirname $0)

if test -e $SRC_DIR/deploy-config.sh; then 
    source $SRC_DIR/deploy-config.sh
fi

rsync $SRC_DIR/target/commontime-api.jar $COMMONTIME_USER@www.commontime.trade:/srv/

ssh $COMMONTIME_USER@www.commontime.trade sudo systemctl restart commontime-api
