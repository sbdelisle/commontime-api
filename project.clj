 (defproject rat "0.1.0-SNAPSHOT"
   :description "Rat, a timebank server"
   :dependencies [[org.clojure/clojure "1.8.0"]
                  [com.taoensso/timbre "4.10.0"]
                  [metosin/compojure-api "1.1.11"]
                  [org.clojure/math.numeric-tower "0.0.4"]
                  [com.layerware/hugsql "0.4.8"]
                  [mvxcvi/alphabase "1.0.0"]
                  [org.postgresql/postgresql "42.1.4"]
                  [org.xerial/sqlite-jdbc "3.7.15-M1"]
                  [com.taoensso/faraday "1.9.0"]
                  [crypto-random "1.2.0"]
                  [clj-http "3.8.0"]
                  [org.clojure/core.async "0.4.474"]
                  [clj-time "0.14.0"]]
   :ring {:handler rat.handler/app}
   :uberjar-name "commontime-api.jar"
   :profiles {:dev {:dependencies [[javax.servlet/javax.servlet-api "3.1.0"]]
                    :plugins [[lein-ring "0.12.0"]]}})
